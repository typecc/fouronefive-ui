import request from '@/utils/request'
export const login = (phonenumber, password) => {
    const data = {
        phonenumber,
        password
    }
    return request({
        url: '/mobileLogin',
        headers: {
            isToken: false,
            repeatSubmit: false
        },
        method: 'post',
        data: data
    })
}

export const register = (data) => {
    return request({
        url: '/mobileRegister',
        headers: {
            isToken: false,
            repeatSubmit: false
        },
        method: 'post',
        data: data
    })
}

export function getInfo() {
    return request({
        url: '/getInfo',
        method: 'get'
    })
}

export function logout() {
    return request({
        url: '/logout',
        method: 'post'
    })
}
