import request from "@/utils/request.js";

export function getExam(examId) {
    return request({
        url: `/activity/v1/exam/${examId}`,
        method: 'get'
    })
}

export function startExam(examId) {
    return request({
        url: `/activity/v1/exam/start/${examId}`,
        method: 'post'
    })
}

export function submitExam(examRecordId, data) {
    return request({
        data,
        url: `/activity/v1/exam/submit/${examRecordId}`,
        method: 'post'
    })
}

export function listRecord(query) {
    return request({
        url: '/activity/v1/exam/record/list',
        method: 'get',
        params: query
    })
}

export function getRecord(recordId) {
    return request({
        url: `/activity/v1/exam/record/${recordId}`,
        method: 'get',
    })
}
