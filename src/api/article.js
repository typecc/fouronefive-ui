import request from '@/utils/request'

export function listArticle(query) {
    return request({
        url: '/activity/v1/index/article/list',
        method: 'get',
        params: query
    })
}

// 查询文章详细
export function getArticle(articleId) {
    return request({
        url: '/activity/v1/index/article/' + articleId,
        method: 'get'
    })
}
