export const setWindowTitle = (title) => {
    document.title = title
}

export const init = () => {
    console.log('init')
    // 监听双指缩放事件
    window.onload = function() {
        document.documentElement.addEventListener('touchstart', function (event) {
            if (event.touches.length > 1) {
                event.preventDefault();
            }
        }, false);
        var lastTouchEnd = 0;
        document.addEventListener('touchend', function(event) {
            var now = Date.now();
            if (now - lastTouchEnd <= 300) {
                event.preventDefault();
            }
            lastTouchEnd = now;
        }, false);
        // 阻止双指放大
        document.addEventListener('gesturestart', function(event) {
            event.preventDefault();
        });
    }
}

export const durationFormat = (ms) => {
    if (!ms) {
        return '0分'
    }
    let sec = ms / 1000
    if (sec < 1) {
        return '0分'
    }
    if (sec < 60) {
        return `${Number.parseInt(sec.toString())}秒`
    }
    let min = sec / 60
    console.log(Number.parseInt(min.toString()))
    if (min < 60) {
        sec = sec % 60
        return `${Number.parseInt(min.toString())}分` + (sec === 0?'':`${Number.parseInt(sec.toString())}秒`)
    }
    return '>1小时'
}

export function tansParams(params) {
    let result = ''
    for (const propName of Object.keys(params)) {
        const value = params[propName];
        var part = encodeURIComponent(propName) + "=";
        if (value !== null && value !== "" && typeof (value) !== "undefined") {
            if (typeof value === 'object') {
                for (const key of Object.keys(value)) {
                    if (value[key] !== null && value[key] !== "" && typeof (value[key]) !== 'undefined') {
                        let params = propName + '[' + key + ']';
                        var subPart = encodeURIComponent(params) + "=";
                        result += subPart + encodeURIComponent(value[key]) + "&";
                    }
                }
            } else {
                result += part + encodeURIComponent(value) + "&";
            }
        }
    }
    return result
}
