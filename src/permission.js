import {setWindowTitle} from "@/utils/fouronefive.js";
import {router} from "@/router/index.js";
import {getToken} from "@/utils/auth.js";
import useUserStore from "@/store/modules/user.js";
import {isRelogin} from "@/utils/request.js";
import {showNotify} from "vant";

const whiteList = ['/login', '/register', '/index']
router.beforeEach((to, from, next) => {
    if (!!to.meta.title) {
        setWindowTitle(to.meta.title)
    } else {
        setWindowTitle(import.meta.env.VITE_BASE_TITLE)
    }
    if (getToken()) {
        if (to.path === '/login') {
            next('/index')
        } else if (whiteList.indexOf(to.path) !== -1) {
            next()
        } else {
            let username = useUserStore().name
            if (!username) {
                isRelogin.show = true
                useUserStore().getInfo()
                    .then(() => {
                        next()
                    })
                    .catch(err => {
                    useUserStore().logOut().then(() => {
                        showNotify({ message: err, type: 'danger' })
                        next({ path: '/' })
                    })
                })
            } else {
                next()
            }
        }
    } else {
        if (whiteList.indexOf(to.path) !== -1) {
            next()
        } else {
            next(`/login?redirect=${to.fullPath}`)
        }
    }
})
