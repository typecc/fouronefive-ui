import { createApp } from 'vue'
import App from './App.vue'
import {router} from "@/router/index.js";
import { Lazyload } from 'vant';
import {init} from "@/utils/fouronefive.js";
import '@/style.css'
import 'vant/es/notify/style'
import 'vant/es/dialog/style'
import 'quill/dist/quill.core.css'
import './permission.js'
import store from "@/store";
init()

const app = createApp(App)
app.use(router)
app.use(Lazyload)
app.use(store)
app.mount('#app')
