import {defineStore} from "pinia";
import {getToken, removeToken, setToken} from "@/utils/auth.js";
import {getInfo, login, logout} from "@/api/login.js";

const useUserStore = defineStore(
    'user',
    {
        state: () => ({
            token: getToken(),
            id: '',
            name: '',
            phonenumber: ''
        }),
        actions: {
            login(userInfo) {
                const phonenumber = userInfo.phonenumber.trim()
                const password = userInfo.password

                return new Promise((resolve, reject) => {
                    login(phonenumber, password).then(res => {
                        setToken(res.token)
                        this.token = res.token
                        resolve()
                    }).catch(error => {
                        reject(error)
                    })
                })
            },
            getInfo() {
                return new Promise((resolve, reject) => {
                    getInfo().then(res => {
                        const user = res.user
                        this.id = user.userId
                        this.name = user.userName
                        this.phonenumber = user.phonenumber
                        resolve(res)
                    }).catch(error => {
                        reject(error)
                    })
                })
            },
            // 退出系统
            logOut() {
                return new Promise((resolve, reject) => {
                    logout(this.token).then(() => {
                        this.token = ''
                        this.id = ''
                        this.name = ''
                        this.phonenumber = ''
                        removeToken()
                        resolve()
                    }).catch(error => {
                        reject(error)
                    })
                })
            }
        }
    }
)

export default useUserStore
