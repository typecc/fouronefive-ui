import {createRouter, createWebHashHistory} from 'vue-router'
const routes = [
    {
      path: '/',
      redirect: '/index'
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/pages/login/index.vue'),
        meta: { title: '登录' },
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/pages/register/index.vue'),
        meta: { title: '注册' },
    },
    {
        path: '/exam/:examId',
        name: 'exam',
        component: () => import('@/pages/exam/index.vue'),
        meta: { title: '答题卡' },
    },
    {
        path: '/record',
        name: 'record',
        component: () => import('@/pages/record/index.vue'),
        meta: { title: '答题记录' },
    },
    {
        path: '/record/detail/:recordId',
        name: 'recordDetail',
        component: () => import('@/pages/record-detail/index.vue'),
        meta: { title: '答题详情' },
    },
    {
        path: '/layer',
        redirect: '/index',
        component: () => import('@/layer/index.vue'),
        meta: { transition: 'slide-left' },
        children: [
            {
                path: '/index',
                name: 'index',
                component: () => import('@/pages/index/index.vue'),
                meta: { title: '首页' },
            },
            {
                path: '/activities',
                name: 'activities',
                component: () => import('@/pages/activities/index.vue'),
                meta: { title: '答题活动' },
            }
        ]
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})


export { router }
