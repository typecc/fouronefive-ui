import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite';
import viteCompression from "vite-plugin-compression";
import {VantResolver} from '@vant/auto-import-resolver';
import path from 'path'

// https://vitejs.dev/config/
function _resolve(dir) {
    return path.resolve(__dirname, dir)
}

export default defineConfig(({ mode, command }) => {
    return {
        server: {
            host: '0.0.0.0',
            proxy: {
                // https://cn.vitejs.dev/config/#server-proxy
                '/dev-api': {
                    target: 'http://localhost:8080',
                    changeOrigin: true,
                    rewrite: (p) => p.replace(/^\/dev-api/, '')
                }
            }
        },
        plugins: [
            vue(),
            Components({
                resolvers: [VantResolver()],
            }),
            viteCompression({
                verbose: true, // 默认即可
                disable: false, //开启压缩(不禁用)，默认即可
                deleteOriginFile: false, //删除源文件
                threshold: 10240, //压缩前最小文件大小
                algorithm: 'gzip', //压缩算法
                ext: '.gz', //文件类型
            })
        ],
        resolve: {
            alias: {
                '@': _resolve('src')
            }
        }
    }
})
